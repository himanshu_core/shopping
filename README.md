# Shopping Cart 

## Author 

  - Himanshu

## Quick start

You must have installed:

  - Apache Server version >= 2.0 .
  - PHP version >= 5.5 or newer is recommended.
  - MySQL >= 5.1 via the mysql (deprecated), mysqli and pdo drivers
  
## Install   

  - Navigate to the repository in Bitbucket.
  - Click the Clone button.
  - Copy the clone command (either the SSH format or the HTTPS).
  - If you are using the SSH protocol, ensure your public key is in Bitbucket and loaded on the local system to which you are cloning.
  - Launch a terminal window.
  - Change to the local directory where you want to clone your repository.
  - Paste the command you copied from Bitbucket, for example:
  
  ```
  $ git clone https://himanshu_core@bitbucket.org/himanshu_core/shopping.git
  ```

## Database

  - Create Database 'shopping'.
  - import root:shopping.sql file in the database.
  - open file root:application/config/database.php.
  - Edit you database detail
 
  ```
  $db['default']['hostname'] = 'localhost';
  
  $db['default']['username'] = 'root';
  
  $db['default']['password'] = '';
  
  $db['default']['database'] = 'shopping';
  
  $db['default']['dbdriver'] = 'mysql';
  
  $db['default']['dbprefix'] = '';
  
  $db['default']['pconnect'] = TRUE;
  
  $db['default']['db_debug'] = TRUE;
  
  $db['default']['cache_on'] = FALSE;
  
  $db['default']['cachedir'] = '';
  
  $db['default']['char_set'] = 'utf8';
  
  $db['default']['dbcollat'] = 'utf8_general_ci';
  
  $db['default']['swap_pre'] = '';
  
  $db['default']['autoinit'] = TRUE;
  
  $db['default']['stricton'] = FALSE;
  
  ```
  
  - Save and Exit 
 

## Run The Project

Now open a browser and navigate here [http://localhost/shopping].